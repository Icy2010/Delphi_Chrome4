program Test;

uses
  Vcl.Forms,
  uCEFApplication,
  Main in 'Main.pas' {MainForm},
  Frame_web in 'Frame_web.pas' {FrameWeb: TFrame};

{$R *.res}

begin
  GlobalCEFApp:= TCefApplication.Create;
  if GlobalCEFApp.StartMainProcess then
  begin
    Application.Initialize;
    Application.MainFormOnTaskbar:= True;
    Application.CreateForm(TMainForm, MainForm);
    Application.Run;
  end;

  GlobalCEFApp.Free;
  GlobalCEFApp:= nil;
end.
