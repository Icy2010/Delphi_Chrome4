unit Frame_web;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  uCEFWinControl,
  uCEFWindowParent,
  uCEFChromium,
  uCEFChromiumWindow,
  uCEFTypes,
  uCEFInterfaces,
  uCEFRequestContext,
  uCEFBufferPanel,
  uCEFConstants,
  uCEFUrlRequestClientComponent;

type
  TOnWebCilck = TProc;
  TFrameWeb = class(TFrame)
    Chromium: TChromium;
    CEFWindowParent: TCEFWindowParent;
    procedure ChromiumRenderCompMsg(var aMessage: TMessage;
      var aHandled: Boolean);
    procedure ChromiumAfterCreated(Sender: TObject; const browser: ICefBrowser);
    procedure ChromiumBeforeClose(Sender: TObject; const browser: ICefBrowser);
    procedure ChromiumClose(Sender: TObject; const browser: ICefBrowser;
      var aAction: TCefCloseBrowserAction);
  private
     FDefaultUrl: string;
     FOnCilck:    TOnWebCilck;
    { Private declarations }
    procedure BrowserCreatedMsg(var aMessage : TMessage); message CEF_AFTERCREATED;
    procedure BrowserDestroyMsg(var aMessage : TMessage); message CEF_DESTROY;
  public
    { Public declarations }
  end;

function CraeteFrameWeb(WinCol:  TWinControl;
                        const aDefaultUrl:  string;
                        aCilck:  TOnWebCilck):Boolean;
implementation
  uses
  uCEFApplication;
{$R *.dfm}

function CraeteFrameWeb(WinCol:  TWinControl;
                        const aDefaultUrl:  string;
                        aCilck:  TOnWebCilck):Boolean;
var
  Frame:  TFrameWeb;
begin
  Frame:= TFrameWeb.Create(WinCol);
  Frame.Parent:= WinCol;
  Frame.Name:= 'frame_' + WinCol.Tag.ToString;
  Frame.Align:= alClient;
  with Frame do
  begin
    FOnCilck:= aCilck;
    Result:= Chromium.CreateBrowser(CEFWindowParent);
    FDefaultUrl:= aDefaultUrl;
  end;
end;

procedure TFrameWeb.ChromiumAfterCreated(Sender: TObject;
  const browser: ICefBrowser);
begin
  PostMessage(Handle, CEF_AFTERCREATED, 0, 0);
end;

procedure TFrameWeb.ChromiumBeforeClose(Sender: TObject;
  const browser: ICefBrowser);
begin
  PostMessage(Handle, WM_CLOSE, 0, 0);
end;

procedure TFrameWeb.ChromiumClose(Sender: TObject; const browser: ICefBrowser;
  var aAction: TCefCloseBrowserAction);
begin
  PostMessage(Handle, CEF_DESTROY, 0, 0);
  aAction:= cbaDelay;
end;

procedure TFrameWeb.ChromiumRenderCompMsg(var aMessage: TMessage;
  var aHandled: Boolean);
begin
  if aMessage.Msg = WM_LBUTTONDBLCLK then
  begin
    if Assigned(FOnCilck)  then
       FOnCilck;
  end;
end;

procedure TFrameWeb.BrowserCreatedMsg(var aMessage : TMessage);
begin
  Chromium.LoadURL(FDefaultUrl);
end;

procedure TFrameWeb.BrowserDestroyMsg(var aMessage : TMessage);
begin
   CEFWindowParent.Free;
end;


end.
