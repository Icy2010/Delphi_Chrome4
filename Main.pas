unit Main;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  uCEFChromiumWindow,
  uCEFChromium,
  Vcl.ExtCtrls,
  uCEFWindowParent,
  uCEFTypes,
  uCEFInterfaces,
  uCEFRequestContext,
  uCEFWinControl,
  Vcl.StdCtrls,
  uCEFBufferPanel,
  uCEFUrlRequestClientComponent;

type
  TMainForm = class(TForm)
    Panel_Right_2: TPanel;
    Panel_Left_2: TPanel;
    Panel_Right_1: TPanel;
    Panel_left_1: TPanel;
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
     AHot:    ATOM;
    { Private declarations }
    procedure SetPanelsRect;
    procedure MaxPanel(Panel:  TPanel);
    procedure WMHOTKEY(var msg: TMessage); message WM_HOTKEY;
  public
    { Public declarations }

  end;

var
  MainForm: TMainForm;

implementation
    uses
    Frame_web,
    uCEFApplication;
{$R *.dfm}

////////////////////////////////////////////////////////////////////////////////

procedure TMainForm.WMHotKey(var msg: TMessage);
begin
  case msg.LParamHi of
    VK_F1: MaxPanel(Panel_left_1);
    VK_F5: SetPanelsRect;
    VK_F6: Application.Terminate;
  end;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  UnregisterHotKey(Handle, AHot);
  GlobalDeleteAtom(AHot);
end;

procedure TMainForm.FormResize(Sender: TObject);
begin
  SetPanelsRect;
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
  if FindAtom('HotKey') = 0 then
     AHot:= GlobalAddAtom('HotKey');

     RegisterHotKey(Handle, AHot, 0, VK_F1);
     RegisterHotKey(Handle, AHot, 0, VK_F5);
     RegisterHotKey(Handle, AHot, 0, VK_F6);

  SetPanelsRect;
  CraeteFrameWeb(Panel_left_1,
  'http://baidu.com',
  procedure
  begin
      MaxPanel(Panel_left_1);
  end
  );

  CraeteFrameWeb(Panel_left_2,
  'http://163.com',
  procedure
  begin
      MaxPanel(Panel_left_2);
  end
  );

  CraeteFrameWeb(Panel_Right_1,
  'http://qq.com',
  procedure
  begin
      MaxPanel(Panel_Right_1);
  end
  );

  CraeteFrameWeb(Panel_Right_2,
  'http://ip138.com',
  procedure
  begin
      MaxPanel(Panel_Right_2);
  end
  );
end;

procedure TMainForm.SetPanelsRect;
var
  AHeight,
  AWidth:   Integer ;
begin
  AHeight:= Self.Height div 2;
  Panel_Left_1.Height:= AHeight;
  Panel_Left_2.Height:= AHeight;
  Panel_Right_1.Height:= AHeight;
  Panel_Right_2.Height:= AHeight;

  AWidth:= Self.Width div 2;
  Panel_Left_1.Width:= AWidth;
  Panel_Right_1.Width:= AWidth;
  Panel_Left_2.Width:= AWidth;
  Panel_Right_2.Width:= AWidth;

  Panel_Left_1.Left:= 0;
  Panel_Left_1.Top:= 0;

  Panel_Right_1.Left:= Panel_Left_1.Width;
  Panel_Right_1.Top:= 0;

  Panel_Left_2.Left:= 0;
  Panel_Left_2.Top:= AHeight;

  Panel_Right_2.Top:= AHeight;
  Panel_Right_2.Left:= Panel_Left_2.Width;
end;

procedure TMainForm.MaxPanel(Panel:  TPanel);
var
  I:     Integer;
begin
  if Panel.Tag = 0 then
  begin
    for I:= 0 to Self.ControlCount-1 do
    begin
      if Self.Controls[I] is TPanel  then
      begin
        with (Self.Controls[I] as TPanel) do
        begin
          Width:= 0;
          Height:= 0;
        end;
      end;
    end;

    Panel.Tag:= 1;
    Panel.Left:= 0;
    Panel.Top:= 0;
    Panel.Height:= Self.Height;
    Panel.Width:= Self.Width;
  end
  else
  begin
    SetPanelsRect;
    Panel.Tag:= 0;
  end;
end;

end.
